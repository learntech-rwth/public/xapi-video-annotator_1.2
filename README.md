xAPI Video Annotator
===

- [NX ReadMe](./nx.md)
- [Get started with development](./development.md)
- [Get started with contributions](./contribution.md)
- [How to deploy](./deployment.md)
  - [Manuel proudction deployment](./deployment.md#Production)

## Start the development

> Ensure the steps in [Get started with development](./development.md) are
> completed

### Start frontend and backend locally

1. Start the backend `yarn nx run server:serve`
2. Start the frontend `yarn nx run pwa:serve`

#### Use the backend with a persistence database

1. Create the file `.env` in the workspace root
2. Add the environment variable `MONGODB_HOST=localhost:27017` to the `.env` filte
3. Start a mongo db container `yarn start:mongo`
4. Start the backend `yarn nx run server:serve`
5. Start the frontend `yarn nx run pwa:serve`

## Usage

When the backend is started with an empty database. The following users will be created:

Username | Password | Role | Environment
--- | --- | --- | ---
admin | admin | any
member | member | member | not production
guest | guest | guest | not production
project | project | admin | not production

When the user `project` is created, two projects with linked videos are created: `seed project` and `test data base`. All annotations created for the first project are stored in memory only. Annotations created for the second project are sent to the defined LRS.

The LRS settings for the "Test Database" project can be changed using the environment variables `TESTING_RRS_URL`, `TESTING_LRS_AUTH_KEY`, and `TESTING_LRS_AUTH_SECRET`.

> When the project is started with a Docker. The environment is always Production and the users `member`, `guest`, `project` are **not** created.

## Environment variables

name | default | description
--- | --- | ---
MONGODB_HOST | mongo | domain or hostname of the mongo DB
GLOBAL_PREFIX | | a global prefix for each residual operation path
PUBLIC_URL | | the public URL of the backend application
COOKIE_SECRET | [random uuid] | secret for cookie signing
JWT_SECRET | [random uuid] | secret for JWT signing
EXPERIENCE_API_VERSION | 1.0.3 | value for the Experience Api header when requesting an LRS server
TESTING_LRS_URL | https://lrs.elearn.rwth-aachen.de | the base URL of a test LRS server
TESTING_LRS_AUTH_KEY | a64b891003d3a1d1b9232800ded323faf75ba0a7 | the auth key of the test LRS server
TESTING_LRS_AUTH_SECRET | 6cea007bc5f63b7b3dd928dcade53626e969608c | the authorization secret of the test LRS server
PROJECT_EXTENSIONS_IRI | https://xapi.elearn.rwth-aachen.de/definitions/videoAnnotator/extensions/context/project | IRI to store the project ID in the context extensions of the statement object
VIDEO_EXTENSIONS_IRI | https://xapi.elearn.rwth-aachen.de/definitions/videoAnnotator/extensions/context/video | IRI for storing the video ID in the context extensions of the statement object
TIMESTAMP_EXTENSIONS_IRI | https://xapi.elearn.rwth-aachen.de/definitions/videoAnnotator/extensions/context/timestamp | IRI for storing the timestamp of the annotation in the context extensions of the statement object
